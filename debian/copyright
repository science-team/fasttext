Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: fasttext
Upstream-Contact:
 Edouard Grave <egrave@fb.com>
 Piotr Bojanowski <bojanowski@fb.com>
 Armand Joulin <ajoulin@fb.com>
 Tomas Mikolov <tmikolov@fb.com>
Source: https://github.com/facebookresearch/fastText.git
Files-Excluded:
  website/static/docs/en/html/*.html
  website/static/docs/en/html/*.md5
  website/static/docs/en/html/*.js
  website/static/docs/en/html/*.css
  website/static/docs/en/html/.classfasttext*
  website/static/docs/en/html/search
  website/static/docs/en/html/bc_s.png
  website/static/docs/en/html/bdwn.png
  website/static/docs/en/html/closed.png
  website/static/docs/en/html/doc.png
  website/static/docs/en/html/doxygen.png
  website/static/docs/en/html/folderclosed.png
  website/static/docs/en/html/folderopen.png
  website/static/docs/en/html/nav_f.png
  website/static/docs/en/html/nav_g.png
  website/static/docs/en/html/nav_h.png
  website/static/docs/en/html/open.png
  website/static/docs/en/html/splitbar.png
  website/static/docs/en/html/sync_off.png
  website/static/docs/en/html/sync_on.png
  website/static/docs/en/html/tab_a.png
  website/static/docs/en/html/tab_b.png
  website/static/docs/en/html/tab_h.png
  website/static/docs/en/html/tab_s.png
Comment: The following files were removed because
  website/static/docs/en/html/*.html: it should be regenerated during building package
  website/static/docs/en/html/*.md5: it should be regenerated during building package
  website/static/docs/en/html/*.js: it should be regenerated during building package
  website/static/docs/en/html/*.css: it should be regenerated during building package
  website/static/docs/en/html/.classfasttext*: it should be regenerated during building package
  website/static/docs/en/html/search: it should be regenerated during building package
  website/static/docs/en/html/bc_s.png: it should be regenerated during building package
  website/static/docs/en/html/bdwn.png: it should be regenerated during building package
  website/static/docs/en/html/closed.png: it should be regenerated during building package
  website/static/docs/en/html/doc.png: it should be regenerated during building package
  website/static/docs/en/html/doxygen.png: it should be regenerated during building package
  website/static/docs/en/html/folderclosed.png: it should be regenerated during building package
  website/static/docs/en/html/folderopen.png: it should be regenerated during building package
  website/static/docs/en/html/nav_f.png: it should be regenerated during building package
  website/static/docs/en/html/nav_g.png: it should be regenerated during building package
  website/static/docs/en/html/nav_h.png: it should be regenerated during building package
  website/static/docs/en/html/open.png: it should be regenerated during building package
  website/static/docs/en/html/splitbar.png: it should be regenerated during building package
  website/static/docs/en/html/sync_off.png: it should be regenerated during building package
  website/static/docs/en/html/sync_on.png: it should be regenerated during building package
  website/static/docs/en/html/tab_a.png: it should be regenerated during building package
  website/static/docs/en/html/tab_b.png: it should be regenerated during building package
  website/static/docs/en/html/tab_h.png: it should be regenerated during building package
  website/static/docs/en/html/tab_s.png: it should be regenerated during building package

Files: *
Copyright: 2016-2020, Facebook, Inc. All rights reserved.
License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Files: wikifil.pl
Copyright: 2016 Matt Mahoney
License: public-domain
 The files tagged with this license contains the following paragraphs:
 .
 This program is released to the public domain.
 .
 Written by Matt Mahoney

Files: debian/*
Copyright:
 2018 TSUCHIYA Masatoshi <tsuchiya@imc.tut.ac.jp>
 2019-2020 Kentaro Hayashi <kenhys@xdump.org>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
